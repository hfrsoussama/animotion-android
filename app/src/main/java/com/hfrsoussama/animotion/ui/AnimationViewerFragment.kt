package com.hfrsoussama.animotion.ui

import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.airbnb.lottie.LottieComposition
import com.airbnb.lottie.LottieCompositionFactory
import com.hfrsoussama.animotion.viewmodel.Action
import com.hfrsoussama.animotion.viewmodel.AnimationPlayState
import com.hfrsoussama.animotion.viewmodel.AnimationPlayWithBackgroundState
import com.hfrsoussama.animotion.viewmodel.AnimationViewerViewModel
import com.hfrsoussama.animotion.viewmodel.ErrorState
import com.hfrsoussama.animotion.R
import com.hfrsoussama.animotion.viewmodel.ViewerState
import com.jaredrummler.android.colorpicker.ColorPickerDialog
import kotlinx.android.synthetic.main.fragment_animation_viewer_layout.*
import java.io.FileInputStream
import java.io.InputStream

private const val URI_KEY = "URI"

class AnimationViewerFragment : Fragment() {

    companion object {
        fun newInstance(uri: Uri) = AnimationViewerFragment().apply {
            arguments = bundleOf(URI_KEY to uri)
        }
    }

    private val sharedViewModel by lazy {
        activity?.run {
            ViewModelProviders.of(this).get(AnimationViewerViewModel::class.java)
        } ?: throw Exception("Parent Activity must have ${AnimationViewerViewModel::class.java.name}")
    }

    private val sharedStateObserver by lazy {
        Observer<ViewerState> { renderState(it) }
    }

    private fun renderState(newState: ViewerState) {
        when (newState) {
            is ErrorState -> applyAnimationErrorState(newState.error)
            is AnimationPlayWithBackgroundState -> applyBackground(newState.color)
            is AnimationPlayState -> applyAnimationState(newState.lottieComposition)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_animation_viewer_layout, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        sharedViewModel.currentState.observe(this, sharedStateObserver)
        val uri: Uri = arguments?.getParcelable(URI_KEY) ?: throw NullPointerException("URI not found !")
        handleUri(uri)
    }

    private fun handleUri(uri: Uri) {
        val fis: InputStream? = openStreamOf(uri)

        LottieCompositionFactory.fromJsonInputStream(fis, uri.toString())
            .addListener { composition ->
                sharedViewModel.currentState.apply { value = value?.consumeAction(Action.ViewAnimation(composition)) }
            }
            .addFailureListener { throwable ->
                sharedViewModel.currentState.apply { value = value?.consumeAction(Action.ShowError(throwable)) }
            }
    }

    private fun openStreamOf(uri: Uri): InputStream? {
        return when (uri.scheme) {
            "file" -> FileInputStream(uri.path)
            "content" -> activity?.contentResolver?.openInputStream(uri)
            else -> throw IllegalArgumentException("Unknown scheme ${uri.scheme}").also { exception ->
                sharedViewModel.currentState.apply { value = value?.consumeAction(Action.ShowError(exception)) }
            }
        }
    }

    private fun applyAnimationState(lottieComposition: LottieComposition) {
        lottieAnimationView.setComposition(lottieComposition)
        actionView.setOnClickListener { ColorPickerDialog.newBuilder().show(activity) }
    }

    private fun applyAnimationErrorState(error: Throwable) {
        lottieAnimationView.setAnimation(R.raw.uh_oh_error)
        actionView.setOnClickListener { Toast.makeText(activity, error.message, Toast.LENGTH_LONG).show() }
    }

    private fun applyBackground(color: Int) {
        animationBackgroundView.setBackgroundColor(color)
    }
}

package com.hfrsoussama.animotion.ui

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.transaction
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.hfrsoussama.animotion.viewmodel.Action
import com.hfrsoussama.animotion.viewmodel.AnimationViewerViewModel
import com.hfrsoussama.animotion.viewmodel.FileSelectedState
import com.hfrsoussama.animotion.viewmodel.InitialState
import com.hfrsoussama.animotion.R
import com.hfrsoussama.animotion.viewmodel.ViewerState
import com.hfrsoussama.animotion.viewmodel.WaitingForFileState
import com.jaredrummler.android.colorpicker.ColorPickerDialogListener
import kotlinx.android.synthetic.main.activity_main.*

private const val REQUEST_OPEN_FILE = 9999

class MainActivity : FragmentActivity(), ColorPickerDialogListener {

    /**
     * This activity's viewModel that should handle a [ViewerState] instance
     */
    private val viewModel by lazy {
        ViewModelProviders.of(this).get(AnimationViewerViewModel::class.java)
    }

    /**
     * The [Observer] that observes [viewModel]'s viewerState and call [renderViewState] when it changes
     */
    private val stateObserver by lazy {
        Observer<ViewerState> { renderViewState(it) }
    }

    /**
     * Called when the viewer state changes to update UI accordingly
     *
     */
    private fun renderViewState(newState: ViewerState) {
        Log.i("ouss", "New #State $newState")
        when (newState) {
            is InitialState -> showInitialStateUi()
            is WaitingForFileState -> openFile()
            is FileSelectedState -> showViewer(newState.uri)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        viewModel.currentState.observe(this, stateObserver)

        if (savedInstanceState == null) {
            intent.data?.let {
                viewModel.currentState.apply { value = value?.consumeAction(Action.SelectedFile(it)) }
            }
        }
    }

    private fun openFile() {
        val intent = Intent(Intent.ACTION_GET_CONTENT).apply {
            type = "*/*"
            addCategory(Intent.CATEGORY_OPENABLE)
        }
        startActivityForResult(Intent.createChooser(intent, "Select a JSON file"), REQUEST_OPEN_FILE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            REQUEST_OPEN_FILE -> handleOpenFileResult(resultCode, data)
        }
    }

    private fun handleOpenFileResult(resultCode: Int, intent: Intent?) {
        when (resultCode) {
            Activity.RESULT_OK -> intent?.data?.let { uri ->
                viewModel.currentState.apply { value = value?.consumeAction(Action.SelectedFile(uri)) }
            }
            Activity.RESULT_CANCELED -> viewModel.currentState.apply { value = value?.consumeAction(Action.BackHome()) }
        }
    }

    private fun showInitialStateUi() {
        supportFragmentManager.transaction {
            replace(rootLayout.id, FileBrowserFragment.newInstance())
        }
    }

    private fun showViewer(uri: Uri) {
        supportFragmentManager.transaction {
            replace(rootLayout.id, AnimationViewerFragment.newInstance(uri))
        }
    }

    override fun onDialogDismissed(dialogId: Int) {
        // Do nothing
    }

    override fun onColorSelected(dialogId: Int, color: Int) {
        viewModel.currentState.apply { value = value?.consumeAction(Action.ApplyBackground(color)) }
    }
}

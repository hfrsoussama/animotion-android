package com.hfrsoussama.animotion.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.hfrsoussama.animotion.viewmodel.Action
import com.hfrsoussama.animotion.viewmodel.AnimationViewerViewModel
import com.hfrsoussama.animotion.R
import kotlinx.android.synthetic.main.fragment_open_file_layout.*

class FileBrowserFragment : Fragment() {

    companion object {
        fun newInstance() = FileBrowserFragment()
    }

    private val sharedViewModel by lazy {
        activity?.run {
            ViewModelProviders.of(this).get(AnimationViewerViewModel::class.java)
        } ?: throw Exception("Parent Activity must have an ${AnimationViewerViewModel::class.java.name}")
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_open_file_layout, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        openFileView.setOnClickListener {
            sharedViewModel.currentState.apply { value = value?.consumeAction(Action.OpenFile()) }
        }
    }
}
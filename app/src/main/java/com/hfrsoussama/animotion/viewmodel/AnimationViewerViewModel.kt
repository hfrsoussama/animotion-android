package com.hfrsoussama.animotion.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class AnimationViewerViewModel : ViewModel() {

    val currentState by lazy {
        MutableLiveData<ViewerState>().apply { setValue(InitialState()) }
    }
}
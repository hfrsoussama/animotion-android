package com.hfrsoussama.animotion.viewmodel

import android.net.Uri
import com.airbnb.lottie.LottieComposition

sealed class Action {
    class OpenFile : Action()
    class SelectedFile(val uri: Uri) : Action()
    class ViewAnimation(val lottieComposition: LottieComposition) : Action()
    class ShowError(val error: Throwable) : Action()
    class BackHome : Action()
    class ApplyBackground(val color: Int) : Action()
}

interface ViewerState {
    fun consumeAction(action: Action): ViewerState

    fun throwIllegalStateException(action: Action): Nothing =
        throw IllegalStateException("Invalid action $action passed to current state $this")
}

class InitialState : ViewerState {
    override fun consumeAction(action: Action): ViewerState {
        return when (action) {
            is Action.OpenFile -> WaitingForFileState()
            is Action.SelectedFile -> FileSelectedState(action.uri)
            else -> throwIllegalStateException(action)
        }
    }
}

class WaitingForFileState : ViewerState {
    override fun consumeAction(action: Action): ViewerState {
        return when (action) {
            is Action.SelectedFile -> FileSelectedState(action.uri)
            is Action.BackHome -> InitialState()
            else -> throwIllegalStateException(action)
        }
    }
}

class FileSelectedState(val uri: Uri) : ViewerState {
    override fun consumeAction(action: Action): ViewerState {
        return when (action) {
            is Action.ShowError -> ErrorState(action.error)
            is Action.ViewAnimation -> AnimationPlayState(action.lottieComposition)
            else -> throwIllegalStateException(action)
        }
    }
}

class ErrorState(val error: Throwable) : ViewerState {
    override fun consumeAction(action: Action): ViewerState {
        return when (action) {
            is Action.BackHome -> InitialState()
            else -> throwIllegalStateException(action)
        }
    }
}

open class AnimationPlayState(val lottieComposition: LottieComposition) : ViewerState {
    override fun consumeAction(action: Action): ViewerState {
        return when (action) {
            is Action.BackHome -> InitialState()
            is Action.ApplyBackground -> AnimationPlayWithBackgroundState(lottieComposition, action.color)
            else -> throwIllegalStateException(action)
        }
    }
}

class AnimationPlayWithBackgroundState(lottieComposition: LottieComposition, val color: Int) : AnimationPlayState(lottieComposition) {
    override fun consumeAction(action: Action): ViewerState {
        return when (action) {
            is Action.ApplyBackground -> AnimationPlayWithBackgroundState(lottieComposition, action.color)
            is Action.BackHome -> InitialState()
            else -> throwIllegalStateException(action)
        }
    }
}